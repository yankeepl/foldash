const fsp = require('fs').promises;
const path = require('path');
const crypto = require('crypto');
const fg = require('fast-glob');

const FRAGMENT_SIZE = 1000;
const LONG_FILE_MIN_BYTES = 10000;

function md5(input) {
  return crypto.createHash('md5').update(input).digest('hex');
}

async function getFileContentFragment(fullFilePath, totalBytes) {
  const fileHandle = await fsp.open(fullFilePath, 'r');
  try {
    const startBuffer = Buffer.alloc(FRAGMENT_SIZE);
    const endBuffer = Buffer.alloc(FRAGMENT_SIZE);

    // reading first FRAGMENT_SIZE from start file
    await fileHandle.read(startBuffer, 0, FRAGMENT_SIZE, 0);
    // reading the last FRAGMENT_SIZE from end of file
    await fileHandle.read(endBuffer, 0, FRAGMENT_SIZE, totalBytes - FRAGMENT_SIZE);
    const result = Buffer.concat([startBuffer, endBuffer]);
    return result;
  } finally {
    await fileHandle.close();
  }
}

async function getFileContent(cwd, filePath, minFileSize) {
  const fullFilePath = path.join(cwd, filePath);
  const totalBytes = (await fsp.stat(fullFilePath)).size;
  if (totalBytes < minFileSize) {
    return; //--->>> skip files
  }
  let contentBuffer;

  if (totalBytes >= LONG_FILE_MIN_BYTES) {
    contentBuffer = await getFileContentFragment(fullFilePath, totalBytes);
  } else {
    contentBuffer = await fsp.readFile(fullFilePath);
  }

  return Buffer.concat([contentBuffer, Buffer.from(`,${totalBytes}`)]);
}

async function calculateFolderHash(globs, cwd, minFileSize = 0) {
  // console.log(`Reading file list from ${cwd}`);
  const filePaths = await fg(globs, { cwd, dot: true });
  // console.log(`Calculating hash for ${cwd}`);

  const filesMd5 = {};
  const sortedFilePaths = filePaths.sort();
  let dirname = null;
  for (const filePath of sortedFilePaths) {
    const newDirname = path.dirname(filePath);
    if (dirname !== newDirname) {
      // console.log(`... ${newDirname}`);
      dirname = newDirname;
    }
    // @TODO es6-promise-pool
    const content = await getFileContent(cwd, filePath, minFileSize);
    if (!content) {
      continue;
    }
    filesMd5[filePath] = md5(content);
  }

  return {
    hash: md5(JSON.stringify(filesMd5)),
    details: filesMd5,
  };
}

module.exports.calculateFolderHash = calculateFolderHash;
