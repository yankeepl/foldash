function parseBoolean(value) {
  if (value === 'true' || value === undefined) {
    return true;
  } else if (value === 'false') {
    return false;
  } else {
    throw new Error('Wrong boolean value');
  }
}
module.exports.parseBoolean = parseBoolean;
