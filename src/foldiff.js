#!/usr/bin/env node

const fsc = require('fs');
const path = require('path');
const { calculateFolderHash } = require('./calculate-folder-hash');
const { parseBoolean } = require('./parse-boolean');
const { program } = require('commander');

program
  .option('--silent', 'Silent all messages except generated hash', false)
  .option('--config <config-file-name>', 'Config file name', '.foldiff.json')
  .option('--ignore-empty [true|false]', 'Ignore empty files', parseBoolean, true)
  .option('--output <hash-file-name>', 'Output diff file', 'diff.json')
  .option('--input <hash-file-name>', 'Input diff file to use instead looking for files');

program.parse(process.argv);
const cmdOptions = program.opts();

// function logMessage(message) {
//   if (!cmdOptions.silent) console.log(message);
// }

function logError(message) {
  // eslint-disable-next-line no-console
  if (!cmdOptions.silent) console.error(message);
}

const configPath = cmdOptions.config;
if (!fsc.existsSync(configPath)) {
  if (!cmdOptions.silent) logError(`Config file not found: ${configPath}`);
  process.exit(1);
}
const folderConfigs = JSON.parse(fsc.readFileSync(configPath).toString());
const outputPath = cmdOptions.output;
const inputPath = cmdOptions.input;

const foldiff = async () => {
  let allFiles = {};
  const minFileSize = cmdOptions.ignoreEmpty ? 1 : 0;
  const exclude = folderConfigs.exclude || [];
  const excludeGlobs = exclude.map((excludePath) => `!${excludePath}`);
  if (inputPath) {
    allFiles = JSON.parse(fsc.readFileSync(inputPath).toString());
  } else {
    for (const folderConfig of folderConfigs.sources) {
      const hashResult = await calculateFolderHash([...folderConfig.globs, ...excludeGlobs], folderConfig.cwd, minFileSize);
      for (const filePath in hashResult.details) {
        const fullFilePath = path.join(folderConfig.cwd, filePath);
        const hashValue = hashResult.details[filePath];
        if (allFiles[hashValue]) {
          allFiles[hashValue].push(fullFilePath);
        } else {
          allFiles[hashValue] = [fullFilePath];
        }
      }

    // console.log(folderConfig.cwd, fullPathDetails);
    }
  }
  // leave duplicate files only
  for(const fileHash in allFiles) {
    if (allFiles[fileHash].length === 1) {
      delete allFiles[fileHash];
      continue; //-->>
    }
    allFiles[fileHash] = allFiles[fileHash].sort((pathA, pathB) => {
      let aWeight = 0;
      let bWeight = 0;
      for(const weightFilePath in folderConfigs.weights) {
        const fileWeight = folderConfigs.weights[weightFilePath];
        if (pathA.startsWith(weightFilePath)) {
          aWeight = Math.max(aWeight, fileWeight);
          // console.log('found A', pathA, weightFilePath, aWeight);
        }
        if (pathB.startsWith(weightFilePath)) {
          bWeight = Math.max(bWeight, fileWeight);
          // console.log('found B', pathB, weightFilePath, bWeight);
        }
      }
      if (bWeight === aWeight) {
        if (path.dirname(pathA) !== path.dirname(pathB) ) {
          // eslint-disable-next-line no-console
          console.warn(`Warning - the same weight ${aWeight}:\n- ${pathA}\n- ${pathB}`);
        }
      }
      return bWeight - aWeight;
    });
  }
  fsc.writeFileSync(outputPath, JSON.stringify(allFiles, null, 2) );
};
foldiff().catch(logError);
