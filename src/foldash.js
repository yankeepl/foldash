#!/usr/bin/env node

const fsc = require('fs');
const path = require('path');
const { calculateFolderHash } = require('./calculate-folder-hash');
const { parseBoolean } = require('./parse-boolean');
const { program } = require('commander');

program
  .description('Calculate folder hash')
  .option('--hash-folder <folder-path>', 'Folder path to be hashed')
  .option('--config <config-file-name>', 'Config file name', '.foldash.json')
  .option('--diff [true|false]', 'Set if hash should be compared with previous one', parseBoolean, true)
  .option('--update', 'Update hash file')
  .option('--silent', 'Silent all messages except generated hash', false)
  .option('--debug [true|false]', 'Debug output', parseBoolean, false)
  .option('--inverse-exit-code', 'Inverse exit code =0 on error and changed, =1 when hash not changed', false)
  .option('--output <hash-file-name>', 'Output hash file name', 'hash.txt');

program.parse(process.argv);
const cmdOptions = program.opts();

function logMessage(message) {
  // eslint-disable-next-line no-console
  if (!cmdOptions.silent) console.log(message);
}

function logError(message) {
  // eslint-disable-next-line no-console
  if (!cmdOptions.silent) console.error(message);
}

const isDiffMode = cmdOptions.diff;
const HASH_CHANGED_EXIT_CODE = isDiffMode ? (cmdOptions.inverseExitCode ? 0 : 1) : 0;
const HASH_NOT_CHANGED_EXIT_CODE = isDiffMode ? (cmdOptions.inverseExitCode ? 1 : 0) : 0;

const configPath = cmdOptions.config;
if (!fsc.existsSync(configPath)) {
  if (!cmdOptions.silent) logError(`Config file not found: ${configPath}`);
  process.exit(1);
}
const folderHashConfig = JSON.parse(fsc.readFileSync(configPath).toString());

const outputPath = cmdOptions.output;
const debugPath = outputPath + '.debug.json';

let previousHash = null;

if (isDiffMode && fsc.existsSync(outputPath)) {
  previousHash = fsc.readFileSync(outputPath).toString();
}

const foldash = async () => {
  const globs = [
    ...folderHashConfig.globs,
    // @TODO ignore only if file is placed in cwd folder && update mode
    '!**/' + path.basename(outputPath), // ignore hash output file
    // @TODO ignore only if file is placed in cwd folder && debug mode
    '!**/' + path.basename(debugPath), // ignore hash debug file
    // @TODO ignore only if file is placed in cwd folder - consider if really config should be omitted?
    '!**/' + path.basename(configPath), // ignore hash debug file
  ];
  const startFolder = cmdOptions.hashFolder || folderHashConfig.cwd || '.';
  const hashResult = await calculateFolderHash(globs, startFolder);
  logMessage(`Hashed ${Object.keys(hashResult.details).length} files.`);
  if (cmdOptions.debug) {
    fsc.writeFileSync(debugPath, JSON.stringify(hashResult, null, 2));
  }
  const newHash = hashResult.hash;
  if (cmdOptions.silent) {
    // eslint-disable-next-line no-console
    console.log(newHash);
  }
  if (previousHash !== newHash) {
    logMessage(`Folder hash changed ${process.cwd()}: ${previousHash} => ${newHash}`);
    if (cmdOptions.update) {
      fsc.writeFileSync(outputPath, newHash);
      logMessage(`Hash file has been updated: ${outputPath}`);
    }
    process.exit(HASH_CHANGED_EXIT_CODE);
  }
  logMessage(`Folder hash not changed ${process.cwd()}:  ${previousHash}`);
  process.exit(HASH_NOT_CHANGED_EXIT_CODE);
};

foldash().catch((err) => {
  logError(err);
});
