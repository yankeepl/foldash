# Foldash

## run:

Add dependency to your package.json:

```json
{
  "devDependencies" {
    "foldash": "git+https://gitlab.com/yankeepl/foldash.git#1.1.1"
  }
}
```

`yarn install`, npm install` ... etc

- run foldash: `yarn foldash --help` to show all cLI options
- run foldiff: `yarn foldiff --help` to show all cLI options

### docker-compose:

```sh
docker-compose run --rm --no-deps -v `pwd`:/app -w /app foldash /usr/bin/foldash
```

### docker

```sh
docker run --rm -v `pwd`:/app -w /app foldash:1.0 foldash
```
