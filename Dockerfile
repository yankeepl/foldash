FROM node:16-alpine as builder
RUN apk add --no-cache make gcc g++ python3

WORKDIR /usr/src/app

ENV NODE_ENV development

COPY ./package.json ./yarn.lock /usr/src/app/
COPY ./src/ /usr/src/app/src/

RUN yarn install && yarn cache clean
RUN yarn pkg -t node16-alpine-x64 --output dist/foldash src/foldash.js

FROM alpine:latest

COPY --from=builder /usr/src/app/dist/foldash /usr/src/app/dist/foldash
RUN ln -s /usr/src/app/dist/foldash /usr/bin/foldash

CMD ["foldash"]
